# Space Runner

Space Runner est un jeu où l'utilisateur contrôle un petit vaisseau spatial.
Le but de ce vaisseau est de parcourir la plus grande distance possible à travers le cosmos, mais sur sa route il croisera des astéroïdes qu'il devra a tout prix éviter s'il ne souhaite pas finir en morceaux.
Il croisera également le chemin de monstres spatiaux qui le stopperont dans son parcours, il devra alors les tuer afin de reprendre sa progression.
Vous avez la possibilité de personnaliser votre vaisseau parmi 8 modèles tous plus beaux les uns que les autres.
Dans les paramètres vous pourrez choisir si vous souhaitez contrôler votre vaisseau via des boutons ou en utilisant directement les mouvements de votre smartphone.
Pour les plus aguerri d'entre vous, il y également 3 niveaux de difficultés, de quoi vous donner du fil à retordre !

Space Runner est un jeu d'obstacles, ce qui signifie que le vaisseau peut se déplacer en haut et en bas tout en évitant les obstacles sur son chemin.
Le but est donc de faire le plus gros score pour enregistrer son plus gros record personnel.
Pour faire le plus grand score, il faut éviter de mourir en se faisant toucher par un astéroïde ou par les projectiles des boss.

Le jeu comprend:
* trois niveaux de difficulté
* des animations
* plusieurs skins (apparences) séléctionnables pour votre vaisseau
* un style graphique homogène
* un boss tout les 10 000 mètres
* un système de sauvegarde de scores/records
* gère correctement la mort du personnage
* fonctionne sur tous les types de smartphone android
* une musique de fond pendant la partie
* la possibilité de couper la musique
* le choix d'utilisation de l'acceleromètre ou des boutons pour le déplacement