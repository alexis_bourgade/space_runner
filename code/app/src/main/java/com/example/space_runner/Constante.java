package com.example.space_runner;

/**
 * Contient les constantes du jeux
 */
public class Constante {
    //champ static pour simplifier l'usage des constantes
    public static int HEIGHT_SCREEN;        //attribué au lancement de l'application
    public static int WIDTH_SCREEN;         //attribué au lancement de l'application
    public final static int MOVE=15;
    public final static int MOVE_BOSS=7;
    public final static float ZONE_NEUTRE= (float) 1.5;
    public final static int DISTANCE=17;
    public final static int VITTESSE_DEP_MISSILE=30;
    public final static int VITTESSE_DEP_BILE=15;
    public static int DEGAT_JOUEUR=5;
    public static int NB_ASTEROIDE=6;                   // /-\
    public static int VITESSE_MIN_ASTEROIDE = 5;        //  |
    public static int VITESSE_MAX_ASTEROIDE = 15;       //  |---valeur de base qui pouront être changer
    public static int TAILLE_MIN_ASTEROIDE = 20;        //  |   à la creation d'un partie SEULEMENT
    public static int TAILLE_MAX_ASTEROIDE = 100;       // \_/
}


