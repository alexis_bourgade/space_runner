package com.example.space_runner.Model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;
import com.example.space_runner.Constante;

public class Vaisseau extends ElementMultiDirectionelle {

    private String nom;
    private int image;

    private boolean boolUp = false;
    private boolean boolDown = false;

    /**
     * Constructeur pour le jeux
     */
    public Vaisseau() { super(); }

    /**
     * Constructeur pour la page de skin
     * @param nom est le nom du vaisseau
     * @param image est l'image du vaisseau
     */
    public Vaisseau(String nom, int image){
        this.nom=nom;
        this.image=image;
    }

    /**
     * permet de dessiner le vaisseau
     * @param canvas est le canvas dans lequel dessiner
     */
    public void draw(Canvas canvas) {
        Log.d("testPossitionVaisseau","yVaisseau :"+this.y);
        if (boolDown && (this.getY() < Constante.HEIGHT_SCREEN - (this.getHeight() * 2))) down();
        if (boolUp && (this.getY() > this.getHeight() >> 1)) up();
        canvas.drawBitmap(this.getBm(), this.x, this.y, null);
    }

    /**
     * Re-dessine le vaisseau à sa position initiale
     * @param canvas est le canvas dans lequel dessiner
     */
    public void drawInit(Canvas canvas){
        canvas.drawBitmap(Bitmap
                .createBitmap(listAnnimation.get(indexBitmap),0,0,listAnnimation.get(indexBitmap)
                        .getWidth(),listAnnimation.get(indexBitmap).getHeight()),this.x,this.y,null);
    }

    /**
     *  Augmente la hauteur du vaisseau
     */
    public void up() { y -= Constante.MOVE; }

    /**
     *  Diminue la hauteur du vaisseau
     */
    public void down() {
        y += Constante.MOVE;
    }

    /**
     * @return une des animation, en lui appliquant une rotation si le vaiseau monte ou descends
     */
    @Override                       //sert surtout si on a des animations
    public Bitmap getBm() {
        conteur++;
        if (conteur == nbAnim) {
            for (int i = 0; i < nbAnim; i++) {
                if (i == nbAnim - 1) {
                    indexBitmap = 0;
                    break;
                } else if (indexBitmap == i) {
                    indexBitmap = i + 1;
                    break;
                }
            }
            conteur = 0;
        }
        if (this.boolDown) {
            Matrix matrix = new Matrix();
            matrix.postRotate(15);
            return Bitmap.createBitmap(listAnnimation.get(indexBitmap), 0, 0, listAnnimation.get(indexBitmap).getWidth(), listAnnimation.get(indexBitmap).getHeight(), matrix, true);
        } else if (this.boolUp) {
            Matrix matrix = new Matrix();
            matrix.postRotate(-15);
            return Bitmap.createBitmap(listAnnimation.get(indexBitmap), 0, 0, listAnnimation.get(indexBitmap).getWidth(), listAnnimation.get(indexBitmap).getHeight(), matrix, true);
        }
        return listAnnimation.get(indexBitmap);
    }

    /**
     * Paramètre boolUp
     * @param boolUp est la valeur à attribuer
     */
    public void setBoolUp(boolean boolUp) {
        this.boolUp = boolUp;
    }

    /**
     * Paramètre boolDown
     * @param boolDown est la valeur à attribuer
     */
    public void setBoolDown(boolean boolDown) {
        this.boolDown = boolDown;
    }

    /**
     * @return le nom du vaisseau
     */
    public String getNom() {
        return nom;
    }

    /**
     * @return l'image du vaisseau
     */
    public int getImage() {
        return image;
    }
}
