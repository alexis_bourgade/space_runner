package com.example.space_runner.Model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import com.example.space_runner.Constante;

public class Missile extends ElementHorizontaux {

    /**
     * Constructeur
     * @param x est la position x du missile
     * @param y est la position y du missile
     */
    public Missile(Float x, Float y){
        super(x,y,Constante.WIDTH_SCREEN/20,Constante.HEIGHT_SCREEN/20);
    }

    /**
     * permet de dessiner un asteroïde
     * @param canvas est le canvas dans lequel dessiner
     */
    public void draw(Canvas canvas){
        x+=Constante.VITTESSE_DEP_MISSILE;
        canvas.drawBitmap(this.bm,this.x,this.y,null);
    }

    /**
     *  Creer une bitmap redimensionné à partir d'une bitmap
     * @param bm est la nouvelle bitmap
     */
    @Override
    public void setBm(Bitmap bm) { this.bm = Bitmap.createScaledBitmap(bm, width, height, true); }

}
