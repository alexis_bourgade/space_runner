package com.example.space_runner.View.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.space_runner.R;


public class PvBarFrag extends Fragment {

    private ProgressBar pvBoss;


    /**
     * Créé le fragment
     * @param inflater contient la vue à se rattacher
     * @param container est un ensemble de view
     * @param savedInstanceState est le Bundle à récuperer
     * @return la View créer
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pv_bar, container, true);
        pvBoss = rootView.findViewById(R.id.pvBar);
        pvBoss.setProgress(100);

        return rootView;
    }
}