package com.example.space_runner.Gestionnaire;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import com.example.space_runner.R;
import com.example.space_runner.Model.Vaisseau;

import java.util.ArrayList;

public class AdaptateurSkin extends ArrayAdapter<Vaisseau> {


    private final Context _context;
    private ArrayList<Vaisseau> vaisseau;

    public AdaptateurSkin(Context context, int resource, ArrayList<Vaisseau> vaisseau) {
        super(context, resource, vaisseau);
        _context = context;
        this.vaisseau = vaisseau;
    }


    @Override
    public View getView(int position, View vaisseauView,  ViewGroup parent) {
        if(vaisseauView == null) {
            LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vaisseauView = inflater.inflate(R.layout.fragment_skin_vaisseau, parent, false);
        }
        ImageView imageView = vaisseauView.findViewById(R.id.imageVaisseau);
        imageView.setBackgroundResource(vaisseau.get(position).getImage());

        TextView viewTitle = vaisseauView.findViewById(R.id.nomVaisseau);
        Log.d("testAdapteteur", vaisseau.get(position).getNom());
        viewTitle.setText(vaisseau.get(position).getNom());
        viewTitle.setTag(vaisseau.get(position).getNom());
        return vaisseauView;
    }
}
