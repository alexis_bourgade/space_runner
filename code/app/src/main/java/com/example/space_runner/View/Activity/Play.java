package com.example.space_runner.View.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.space_runner.Constante;
import com.example.space_runner.Gestionnaire.GestionnaireAccelero;
import com.example.space_runner.Gestionnaire.GestionnaireNiveau;
import com.example.space_runner.R;
import com.example.space_runner.Gestionnaire.GestionnaireSauvegarde;
import com.example.space_runner.View.GameMaster;


public class Play extends AppCompatActivity {

    private boolean isMusicActive;
    private boolean isAccelerometreActive;
    private GestionnaireAccelero gestioAccelero;
    private GestionnaireSauvegarde gestioSauvegarde;
    private SensorManager acceleroManager;
    private GameMaster gameMaster;
    private View gameOver,fragAffichageBoss;
    private TextView distanceParcouru, bestScore, score, txt_gameOver;
    private Button upButton, downButton, startButton, retourMenu, reJouer;
    private MediaPlayer mediaPlayer;
    private ProgressBar barPv;


    /**
     * Constructeur
     * @param savedInstanceState est le Bundle à charger
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new GestionnaireNiveau(this, this.getIntent().getIntExtra("niveau",1));
        gestioSauvegarde = new GestionnaireSauvegarde(this,"gameSetting", Context.MODE_PRIVATE);

        isMusicActive = gestioSauvegarde.getValueBoolSharedPreference("volumeIsActivated",true);
        isAccelerometreActive = gestioSauvegarde.getValueBoolSharedPreference("acceleroMetreIsAcitvated",false);

        if (isMusicActive){
            this.mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.musique_play);
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }


        if (isAccelerometreActive) {
            acceleroManager = (SensorManager) getSystemService(SENSOR_SERVICE);

            if (acceleroManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null) {
                Log.d("accelero", "accelero issue");
                isAccelerometreActive = false;
            }
        }
        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        Constante.HEIGHT_SCREEN = dm.heightPixels;
        Constante.WIDTH_SCREEN = dm.widthPixels;
        setContentView(R.layout.activity_play);

        gameMaster = findViewById(R.id.gameView1);
        gameMaster.setPlay(this);
        fragAffichageBoss = findViewById(R.id.fragmentPvBar);
        fragAffichageBoss.setVisibility(View.INVISIBLE);
        gameOver = findViewById(R.id.fragmentGameOver);

        upButton = findViewById(R.id.buttonUP);
        downButton = findViewById(R.id.buttonDown);
        distanceParcouru = findViewById(R.id.textDistance);

        startButton = gameOver.findViewById(R.id.startButton);
        bestScore = gameOver.findViewById(R.id.bestScore);
        score = gameOver.findViewById(R.id.score);
        txt_gameOver = gameOver.findViewById(R.id.txt_gameOver);
        retourMenu = gameOver.findViewById(R.id.buttonMenu);
        reJouer = gameOver.findViewById(R.id.buttonReJouer);

        barPv = fragAffichageBoss.findViewById(R.id.pvBar);


        if (isAccelerometreActive) {
            upButton.setVisibility(View.INVISIBLE);
            downButton.setVisibility(View.INVISIBLE);
            gestioAccelero = new GestionnaireAccelero(acceleroManager, gameMaster);
        }
        else {
            upButton.setOnTouchListener((View v, MotionEvent event) -> {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    gameMaster.getVaisseau().setBoolUp(true);
                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    gameMaster.getVaisseau().setBoolUp(false);
                }
                return true;
            });

            downButton.setOnTouchListener((View v, MotionEvent event) -> {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    gameMaster.getVaisseau().setBoolDown(true);
                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    gameMaster.getVaisseau().setBoolDown(false);
                }
                return true;
            });
        }

        reJouer.setOnTouchListener((view, motionEvent) -> {
            if(isAccelerometreActive) gestioAccelero.setPositionDeBase();
            gameMaster.setDistanceParcouru(0);
            afficherGameOver(View.INVISIBLE);
            gameMaster.initVaisseau();
            gameMaster.initAsteroide();
            gameMaster.initBossMuta();
            gameMaster.setAttaqueActive(false);
            gameMaster.setRun(true);
            fragAffichageBoss.setVisibility(View.INVISIBLE);
            distanceParcouru.setVisibility(View.VISIBLE);
            return false;
        });

        retourMenu.setOnTouchListener((view, motionEvent) -> {
            this.finish();
            return false;
        });

        startButton.setOnClickListener(v -> {
            gameMaster.setRun(true);
            if(isAccelerometreActive) gestioAccelero.setPositionDeBase();
            startButton.setVisibility(View.INVISIBLE);
        });
    }

    /**
     * Affiche à l'écran la distance parcouru par le joueur
     * @param distanceParcouru est la distance à afficher
     */
    public void setDistanceParcouru(String distanceParcouru) {
        this.distanceParcouru.setText(distanceParcouru);
    }

    /**
     * Modifie l'affichage pour faire apparaitre ou disparaitre les differents affichages du boss
     * @param visibilite est la visibilité souhaité du boss
     */
    public void affichageBoss(int visibilite){
        if (visibilite==View.VISIBLE){
            distanceParcouru.setVisibility(View.INVISIBLE);
            fragAffichageBoss.setVisibility(View.VISIBLE);
        }
        else {
            distanceParcouru.setVisibility(View.VISIBLE);
            fragAffichageBoss.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Modifie l'affichage pour faire apparaitre ou disparaitre les differents affichages du Game Over
     * @param visibilite est la visibilité souhaité de l'affichage
     */
    @SuppressLint("SetTextI18n")
    public void afficherGameOver(int visibilite){
        if (gameMaster.isRun()) {
            score.setText(String.valueOf(gameMaster.getDistanceParcouru()));
            bestScore.setText("Meilleur score : "+ gameMaster.getBestScore());
        }
        bestScore.setVisibility(visibilite);
        score.setVisibility(visibilite);
        txt_gameOver.setVisibility(visibilite);
        retourMenu.setVisibility(visibilite);
        reJouer.setVisibility(visibilite);
    }

    /**
     * actualise la barre de vie du boss
     */
    public void actualiserBarrePv(){
        barPv.setProgress((int) gameMaster.getPvBoss());
    }

    /**
     * lors de la reprise de l'application :
     *  - si l'acceleromètre est activé relance la prise de position de l'acceleromètre et re-calcul sa position de base
     *  - si la musique est activé, la reprend là où elle c'etait arrété (recommence si pas lancé avant)
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (isAccelerometreActive) {
            acceleroManager.registerListener(gestioAccelero.getAcceleroEventListener(), gestioAccelero.getAccelero(), SensorManager.SENSOR_DELAY_FASTEST);
            gestioAccelero.setPositionDeBase();
        }
        if (isMusicActive) mediaPlayer.start();
    }

    /**
     * lors de la mise en pause de l'application :
     *  - si l'acceleromètre est activé stop la prise de position de l'acceleromètre
     *  - si la musique est activé, la met en pause
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (isAccelerometreActive){
            acceleroManager.unregisterListener(gestioAccelero.getAcceleroEventListener());
        }
        if (isMusicActive) mediaPlayer.pause();
    }
}