package com.example.space_runner.View.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.space_runner.R;
import com.example.space_runner.Gestionnaire.GestionnaireSauvegarde;
@SuppressLint("UseCompatLoadingForDrawables")
public class Option extends AppCompatActivity {
    private ImageButton buttonAccel;
    private ImageButton buttonVolume;
    private boolean buttonAccelState;
    private boolean buttonVolumeState;
    private GestionnaireSauvegarde gestionnaireSauvegarde;


    /**
     *
     * @param savedinstanceState est le Bundle à charger
     */
    @Override
    protected void onCreate(@Nullable Bundle savedinstanceState){
        super.onCreate(savedinstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.option);
        gestionnaireSauvegarde = new GestionnaireSauvegarde(this,"gameSetting", Context.MODE_PRIVATE);

        buttonAccelState=gestionnaireSauvegarde.getValueBoolSharedPreference("acceleroMetreIsAcitvated",true);
        buttonVolumeState=gestionnaireSauvegarde.getValueBoolSharedPreference("volumeIsActivated",true);

        buttonAccel = findViewById(R.id.buttonAccel);
        if (buttonAccelState){
            buttonAccel.setImageDrawable(getResources().getDrawable(R.drawable.accel_icon));
        }
        else{
            buttonAccel.setImageDrawable(getResources().getDrawable(R.drawable.non_accel_icon));
        }
        buttonAccel.setOnClickListener(imgButtonHandler);

        buttonVolume =findViewById(R.id.buttonVolume);
        if (buttonVolumeState){
            buttonVolume.setImageDrawable(getResources().getDrawable(R.drawable.volume_on_icon));
        }
        else{
            buttonVolume.setImageDrawable(getResources().getDrawable(R.drawable.volume_off_icon));
        }
        buttonVolume.setOnClickListener(imgButtonHandler2);
    }

    View.OnClickListener imgButtonHandler = new View.OnClickListener() {

        /**
         * Active ou desactive l'utilisation de l'acceleromètre
         * @param v est la view où se trouve le bouton
         */

        public void onClick(View v) {
            if (buttonAccelState) { //on dit que false desactive l'acceleromètre ? PARFAIT on est d'accord
                buttonAccel.setImageDrawable(getResources().getDrawable(R.drawable.non_accel_icon));
                gestionnaireSauvegarde.editorPutBool("acceleroMetreIsAcitvated",false);
                buttonAccelState=false;
            } else {
                buttonAccel.setImageDrawable(getResources().getDrawable(R.drawable.accel_icon));
                gestionnaireSauvegarde.editorPutBool("acceleroMetreIsAcitvated",true);
                buttonAccelState=true;
            }
        }
    };

    View.OnClickListener imgButtonHandler2 = new View.OnClickListener() {
        /**
         * Active ou desactive le son de l'application
         * @param v est la View où se trouve le bouton
         */
        public void onClick(View v) {
            if (buttonVolumeState) {
                buttonVolume.setImageDrawable(getResources().getDrawable(R.drawable.volume_off_icon));
                gestionnaireSauvegarde.editorPutBool("volumeIsActivated",false);
                buttonVolumeState=false;
            } else {
                buttonVolume.setImageDrawable(getResources().getDrawable(R.drawable.volume_on_icon));
                gestionnaireSauvegarde.editorPutBool("volumeIsActivated",true);
                buttonVolumeState=true;
            }
        }
    };

    /**
     * Definie l'action du bouton "menu" pour fermer l'activité courant
     * @param view est la View où se trouve le bouton
     */
    public void clicMenu(View view) {
        finish();
    }
}
