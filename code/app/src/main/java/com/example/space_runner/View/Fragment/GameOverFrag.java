package com.example.space_runner.View.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.example.space_runner.R;


public class GameOverFrag extends Fragment {

    private Button startButtton;

    /**
     * Créé le fragment
     * @param inflater contient la vue à se rattacher
     * @param container est un ensemble de view
     * @param savedInstanceState est le Bundle à récuperer
     * @return la View créer
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_game_over, container, true);
        startButtton = rootView.findViewById(R.id.startButton);

        startButtton.setOnClickListener(v -> startButtton.setVisibility(View.INVISIBLE));

        return rootView;
    }
}