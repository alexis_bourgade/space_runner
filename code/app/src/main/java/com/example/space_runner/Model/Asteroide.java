package com.example.space_runner.Model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import com.example.space_runner.Constante;

public class Asteroide extends ElementHorizontaux {

    private int vitesseDep;

    /**
     * Constructeur
     */
    public Asteroide(){
        super(Constante.WIDTH_SCREEN,0,1,1);
        setRandomVitesseDep();
        randomYDepart();
        setHeight();
    }

    /**
     * permet de dessiner un asteroïde
     * @param canvas est le canvas dans lequel dessiner
     */
    public void draw(Canvas canvas){
        x-=vitesseDep;
        canvas.drawBitmap(this.bm,this.x,this.y,null);
    }

    /**
     * permet de set une vitesse de déplacement aleatoire en fonction des valeur ecrite dans les constantes
     */
    public void setRandomVitesseDep(){ vitesseDep= ((int) (Constante.VITESSE_MIN_ASTEROIDE + (Math.random() * (Constante.VITESSE_MAX_ASTEROIDE - Constante.VITESSE_MIN_ASTEROIDE))))* Constante.WIDTH_SCREEN/1000; }

    /**
     * permet de set une taille aleatoire en fonction des valeur ecrite dans les constantes
     */
    public void setHeight(){ width=height=(int)(Constante.TAILLE_MIN_ASTEROIDE + (Math.random() * (Constante.TAILLE_MAX_ASTEROIDE - Constante.TAILLE_MIN_ASTEROIDE))); }

    /**
     * Creer une bitmap redimentionné à partir d'un bitmap de base
     * @param bm est la bitmap à redimentionné
     */
    @Override
    public void setBm(Bitmap bm) { this.bm = Bitmap.createScaledBitmap(bm, width, height, true); }
}
