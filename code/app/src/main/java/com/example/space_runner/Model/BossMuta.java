package com.example.space_runner.Model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;
import com.example.space_runner.Constante;


public class BossMuta extends ElementMultiDirectionelle {
    private int niveau;
    private float pv;
    private boolean bossAcitve=false;

    private boolean boolArrive=false, boolPart=false;

    /**
     * Constructeur
     */
    public BossMuta(){
        niveau=1;
        conteur=0;
        indexBitmap=0;
        pv=100;
    }

    /**
     * permet de dessiner un asteroïde
     * @param canvas est le canvas dans lequel dessiner
     */
    public void draw(Canvas canvas){
        if (boolArrive){arrive();}
        if (boolPart){part();}
        canvas.drawBitmap(this.getBm(),this.x,this.y,null);
    }


    /**
     * @return l'animation suivante celle courante
     */
    @Override
    public Bitmap getBm() {
        conteur++;
        if (conteur==nbAnim){
            for(int i=0; i<nbAnim; i++){
                if (i==nbAnim-1){
                    this.indexBitmap =0;
                    break;
                }
                else if (this.indexBitmap==i){
                    indexBitmap=i+1;
                    break;
                }
            }
            conteur=0;
        }
        return this.listAnnimation.get(indexBitmap);
    }

    /**
     * Fait entrer le boss sur l'ecran
     */
    public void arrive(){
        Log.d("testBoss","left");
        x -= Constante.MOVE_BOSS;
    }

    /**
     * augment le niveau du boss de 1
     */
    public void lvlUp(){ niveau++; }

    /**
     * fait sortir le boss de l'ecran
     */
    public void part(){
        x+= Constante.MOVE_BOSS;
        y+= Constante.MOVE_BOSS;
    }

    /**
     * Re-initialise le boss pour la prochaine vague
     */
    public void resetBoss(){
        this.pv=100;
        x=Constante.WIDTH_SCREEN;
        y=(Constante.HEIGHT_SCREEN >> 1) - (height >> 1);
    }

    /**
     * inflige des dégat au boss
     */
    public void prendDegat(){
        float d = (float)Constante.DEGAT_JOUEUR/niveau;
        if (d < 1) d=1;
        pv-=d;
    }

    /**
     * Set la valeur de "boolArrive"
     */
    public void setArrive(boolean boolArrive) { this.boolArrive = boolArrive; }

    /**
     * Set la valeur de "boolpart"
     */
    public void setPart(boolean boolPart){ this.boolPart=boolPart;}

    /**
     * @return les pv du boss
     */
    public float getPv() { return pv; }

    /**
     * @return le niveau du boss
     */
    public int getNiveau() { return niveau; }

    /**
     * @return vrai si le boss est activé, faux sinon
     */
    public boolean isBossAcitve() { return bossAcitve; }

    /**
     * Set la valeur de "bossActive"
     */
    public void setBossAcitve(boolean bossAcitve) { this.bossAcitve = bossAcitve; }
}
