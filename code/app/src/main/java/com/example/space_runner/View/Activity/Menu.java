package com.example.space_runner.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.space_runner.R;

public class Menu extends AppCompatActivity {

    private boolean selectionneUnNiveau=false;

    /**
     * Constructeur
     * @param savedinstanceState est le Bundle à charger
     */
    @Override
    protected void onCreate(@Nullable Bundle savedinstanceState){
        super.onCreate(savedinstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if(savedinstanceState != null && savedinstanceState.getBoolean("layout")){
            selectionneUnNiveau=true;
            setContentView(R.layout.level_select);
        }else {
            setContentView(R.layout.menu);
        }
        Log.d("testSave","onCreate");
    }

    /**
     * Defini l'action du bouton "Quitter"
     * @param sender est la view où se trouve le bouton
     */
    public void clicExit(View sender){
        finish();
    }

    /**
     * Defini l'action du bouton "Skin"
     * @param view est la view où se trouve le bouton
     */
    public void clicSkin(View view) {
        Intent skinActivity = new Intent(this, Skin.class);
        startActivity(skinActivity);
    }

    /**
     * Defini l'action du bouton "Paramètre"
     * @param view est la view où se trouve le bouton
     */
    public void clicOption(View view) {
        Intent optionActivity = new Intent(this, Option.class);
        startActivity(optionActivity);
    }

    /**
     * Defini l'action du bouton "Play"
     * @param view est la view où se trouve le bouton
     */
    public void clickPlay(View view){
        selectionneUnNiveau=true;
        setContentView(R.layout.level_select);
    }

    /**
     * Gere la selection du niveau
     * @param view est la vue courante
     */
    public void selecLevel(View view){
        Intent playActivity = new Intent(this, Play.class);
        switch(view.getId()){
            case R.id.normal:
                playActivity.putExtra("niveau",1);
                break;
            case R.id.difficile:
                playActivity.putExtra("niveau",2);
                break;
            default:
                playActivity.putExtra("niveau",0);
        }
        startActivity(playActivity);
    }

    /**
     * Defini l'action du bouton "Retour" du layout "level_select.xml"
     * @param view est la view où se trouve le bouton
     */
    public void retour(View view){
        selectionneUnNiveau=false;
        setContentView(R.layout.menu);
    }

    /**
     * Sauvegarde la page actuelle de l'utilisateur en cas de fermeture de l'application
     * @param outState est le Bundle à sauvegarder
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        Log.d("testSave", "onSaveInstanceState"+selectionneUnNiveau);
        outState.putBoolean("layout",selectionneUnNiveau);
        super.onSaveInstanceState(outState);
    }

    /**
     * Restaure la dernière instance de l'application
     * @param savedInstanceState est le Bundle à charger
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("testSave","onRestoreInstanceState");
    }

    /**
     * Redefini l'action finish() dans le menupour que l'application effectue un retour au
     * lieu de se fermer
     */
    @Override
    public void finish() {
        if (selectionneUnNiveau) {
            selectionneUnNiveau = false;
            setContentView(R.layout.menu);
        }
        else super.finish();
    }
}
