package com.example.space_runner.Gestionnaire;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import com.example.space_runner.Constante;
import com.example.space_runner.View.GameMaster;


public class GestionnaireAccelero  {
    private final Sensor accelero;
    private final SensorEventListener acceleroEventListener;
    private float positionDeBase=-1000, valeur, xBase, zBase;

    /**
     * Classe qui permet la gestion de l'acceleromètre
     * @param acceleroManager : permet de recuperer les differents capteur (ici, l'acceleromètre)
     * @param game : permet d'effectuer de donner des action dans la GameView
     */
    public GestionnaireAccelero(SensorManager acceleroManager, GameMaster game){
        accelero = acceleroManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        acceleroEventListener = new SensorEventListener() {
            /**
             * detecte un changement de valeur du capteur
             * @param event : contient differente information du l'evenement
             */
            @Override
            public void onSensorChanged(SensorEvent event) {
                Log.d("valAccel", "position de base : "+ positionDeBase+ " position actuel :"+ valeur);
                if (positionDeBase==-1000){
                    xBase = event.values[0];
                    zBase = event.values[2];
                    positionDeBase = calculeMove(xBase,zBase);
                }

                valeur= calculeMove(event.values[0],event.values[2]);
                if (valeur < positionDeBase- Constante.ZONE_NEUTRE) {
                    game.getVaisseau().setBoolDown(true);
                } else if (positionDeBase-Constante.ZONE_NEUTRE < valeur && valeur < positionDeBase+Constante.ZONE_NEUTRE) {
                    game.getVaisseau().setBoolDown(false);
                    game.getVaisseau().setBoolUp(false);
                } else if (valeur > positionDeBase+Constante.ZONE_NEUTRE) {
                    game.getVaisseau().setBoolUp(true);
                }
            }
            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) { }
        };
    }

    /**
     * permet de calculer le sens de l'inclinaison de l'appareil
     * @param x : la valeur x de l'acceleromètre
     * @param z : la valeur y de l'acceleromètre
     * @return l'inclinaison calculer de l'appareil
     */
    public float calculeMove(float x, float z){
        if (valIsPos(z)){
            return (float) 9.81-x;
        }
        return (float) -9.81+x;
    }

    /**
     * Determine si la valeur est positive
     * @param v est la valeur à calculer
     * @return true si v est positif, false sinon
     */
    public boolean valIsPos(float v){
        return v > 0;
    }

    /**
     *  va permettre au programme de reset la valeur de base de l'acceleromètre
     */
    public void setPositionDeBase() {
        positionDeBase = -1000;
    }

    /**
     * @return le capteur acceleromètre
     */
    public Sensor getAccelero() {
        return accelero;
    }

    /**
     * @return l'EventListener
     */
    public SensorEventListener getAcceleroEventListener() {
        return acceleroEventListener;
    }
}
