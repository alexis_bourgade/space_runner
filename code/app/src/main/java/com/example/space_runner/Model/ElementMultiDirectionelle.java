package com.example.space_runner.Model;

import android.graphics.Bitmap;
import java.util.ArrayList;

public abstract class ElementMultiDirectionelle extends ElementHorizontaux{
    protected int conteur, indexBitmap;
    protected ArrayList<Bitmap> listAnnimation;
    protected int nbAnim;

    /**
     * Constructeur vide
     */
    public ElementMultiDirectionelle(){ super(); }

    /**
     * Constructeur avec argument
     * @param x est la position x sur l'ecran
     * @param y est la position y sur l'ecran
     * @param width est la largeur de l'élément
     * @param height est la hauteur de l'élément
     */
    public ElementMultiDirectionelle(float x, float y, int width, int height){ super(x,y,width,height);}

    /**
     * Créé la liste d'animation de l'élément
     * @param listDep est la nouvelle liste d'animation de deplacement
     */
    public void setListAnnimation(ArrayList<Bitmap> listDep) {
        this.listAnnimation = listDep;
        for (int i=0; i<listDep.size();i++){
            this.listAnnimation.set(i, Bitmap.createScaledBitmap(this.listAnnimation.get(i),this.width,this.height,true));
        }
        nbAnim=listAnnimation.size();
    }
}
