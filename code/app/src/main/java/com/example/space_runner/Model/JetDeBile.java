package com.example.space_runner.Model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;
import static java.lang.Math.atan;

import com.example.space_runner.Constante;


public class JetDeBile extends ElementMultiDirectionelle {
    private double yDeplacement;
    private double yArrive;
    private double degreInclinaison=0;
    private final float xDepart,xTotalDeplacement,yDepart;

    /**
     * Constructeur
     * @param x est la position x du jet de bile
     * @param yArrive est la position y d'arrivé du jet de bile
     * @param muta est le boss d'ou provient le jet de bile
     */
    public JetDeBile(Float x, Float yArrive, BossMuta muta) {
        super(x, 0,Constante.WIDTH_SCREEN/20,Constante.HEIGHT_SCREEN/20);
        xDepart=x;
        yDepart=muta.getY()+(float)(muta.getHeight()*15/18);
        xTotalDeplacement =Constante.WIDTH_SCREEN-(Constante.WIDTH_SCREEN-xDepart);
        resetPosition(yArrive);
    }

    /**
     * @return la bitmap du jet de bile
     */
    @Override
    public Bitmap getBm() { return bm; }

    /**
     *  Permet d'incliner l'image du jet de bile
     * @param bm est la bitmap à incliner
     */
    public void setBmMatrixed(Bitmap bm) {
        Matrix matrix = new Matrix();
        matrix.postRotate((float)degreInclinaison);
        this.bm= Bitmap.createBitmap(Bitmap.createScaledBitmap(bm, width, height, true), 0, 0, width, height, matrix, true);
    }

    /**
     * permet de dessiner un asteroïde
     * @param canvas est le canvas dans lequel dessiner
     */
    public void draw(Canvas canvas){
        x-=(float) Constante.VITTESSE_DEP_BILE;
        y+=yDeplacement;
        canvas.drawBitmap(getBm(),this.x,this.y,null);
    }

    /**
     * Place le jet de bile à la position initial, et paramètre sa cible
     * @param yVaisseau est la positon du vaisseau à cibler
     */
    public void resetPosition(Float yVaisseau){
        this.y= yDepart;
        this.x= xDepart;
        yArrive= yVaisseau;
        yDeplacement = (yArrive-yDepart)/((xTotalDeplacement)/Constante.VITTESSE_DEP_BILE);
        degreInclinaison= atan((yDepart-yArrive)/xTotalDeplacement)*60;
        Log.d("testDeplacement","angle bile: "+degreInclinaison);
    }
}
