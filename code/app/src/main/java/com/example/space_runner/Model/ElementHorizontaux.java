package com.example.space_runner.Model;

import android.graphics.Bitmap;
import android.graphics.Rect;
import com.example.space_runner.Constante;

public abstract  class ElementHorizontaux {
    protected float x, y;
    protected int width, height;
    protected Bitmap bm;

    /**
     * Constructeur vide
     */
    public ElementHorizontaux(){}

    /**
     * Constructeur avec argument
     * @param x est la position x sur l'ecran
     * @param y est la position y sur l'ecran
     * @param width est la largeur de l'élément
     * @param height est la hauteur de l'élément
     */
    public ElementHorizontaux(float x, float y, int width, int height){
        this.x=x;
        this.y=y;
        this.width=width;
        this.height=height;
    }

    /**
     * Donne une valeur y aléatoire
     */
    public void randomYDepart() { this.y=((int) (Math.random() * (Constante.HEIGHT_SCREEN))); }

    /**
     * @return la coordonnée x de l'élément
     */
    public float getX() { return x; }

    /**
     * Set la position x
     * @param x est la nouvelle position y
     */
    public void setX(float x) { this.x = x; }

    /**
     * @return la coordonnée y de l'élément
     */
    public float getY() { return y; }

    /**
     * Set la position y
     * @param y est la nouvelle position y
     */
    public void setY(float y) { this.y = y; }

    /**
     * @return la largeur y de l'élément
     */
    public int getWidth() { return width; }

    /**
     * Set la largeur de l'élément
     * @param width est la nouvelle largeur
     */
    public void setWidth(int width) { this.width = width; }

    /**
     * @return la hauteur y de l'élément
     */
    public int getHeight() { return height; }

    /**
     * Set la hauteur de l'élément
     * @param height est la nouvelle hauteur
     */
    public void setHeight(int height) { this.height = height; }

    /**
     * @return la bitmap y de l'élément
     */
    public Bitmap getBm() { return bm; }

    /**
     * Set la bitmap de l'élément
     * @param bm est la nouvelle bitmap
     */
    public void setBm(Bitmap bm) { this.bm = bm; }

    /**
     * @return le rectangle de l'élément
     */
    public Rect getRect() {
        return new Rect((int)this.x, (int) this.y, (int) this.x+this.width,(int) this.y+this.height);
    }
}
