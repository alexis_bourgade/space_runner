package com.example.space_runner.Gestionnaire;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class GestionnaireSauvegarde {

    private final SharedPreferences sp;

    /**
     * Constructeur
     * @param context est le context de l'application
     * @param nom est le nom donné au fichier à paramètrer
     * @param mode est le mode de visibilité des données sauvegardé
     */
    public GestionnaireSauvegarde(Context context, String nom, int mode){
        sp = context.getSharedPreferences(nom, mode);
    }

    /**
     * @return le SharedPreferences
     */
    public SharedPreferences getSharedPreferences() {
        return sp;
    }


    //gestion des int

    /**
     * Retourne le nombre correspondant à la cle donnée en paramètre
     * @param key est la cle
     * @param defautValue est la valeur par défaut si la cle ne correspond à aucune valeur ou si
     *                    elle est incorrecte.
     * @return le nombre trouvé, defautValue sinon
     */
    public int getValueIntSharedPreference(String key, int defautValue){ return sp.getInt(key,defautValue); }

    /**
     * permet d'ajouter un nombre dans l'editeur
     * @param key est la cle
     * @param value est le nombre que l'on souhaite associer à la cle
     */
    public void editorPutInt(String key, int value){
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(key,value);
        editor.apply();
    }

    //gestion des bool
    /**
     * Retourne le booleen correspondant à la cle donnée en paramètre
     * @param key est la cle
     * @param defautValue est la valeur par défaut si la cle ne correspond à aucune valeur ou si
     *                    elle est incorrecte
     * @return le booleen trouvé, defautValue sinon
     */
    public boolean getValueBoolSharedPreference(String key, boolean defautValue){ return sp.getBoolean(key,defautValue); }

    /**
     * permet d'ajouter un nombre dans l'editeur
     * @param key est la cle
     * @param value est la booleen que l'on souhaite associer à la cle
     */
    public void editorPutBool(String key, boolean value){
        Log.d("etatGyroscope", String.valueOf(value));
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }
}
