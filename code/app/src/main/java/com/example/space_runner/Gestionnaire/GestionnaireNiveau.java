package com.example.space_runner.Gestionnaire;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;
import com.example.space_runner.Constante;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class GestionnaireNiveau {

    String json;

    /**
     *  paramètre le niveau en fonction du choix du joueur
     * @param context est le context de l'application courante
     * @param niveau est le niveau choisi par le joueur
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public GestionnaireNiveau(Context context, int niveau){
            Log.d("testpers","------------------");

            try {
                InputStream inputStream = context.getAssets().open("niveau.json");
                byte[] buffer = new byte[inputStream.available()];
                inputStream.read(buffer);
                inputStream.close();
                json = new String(buffer, StandardCharsets.UTF_8);
                JSONArray jsonArray = new JSONArray(json);

                JSONObject obj = jsonArray.getJSONObject(niveau);
                Constante.NB_ASTEROIDE=obj.getInt("nbAste");
                Log.d("testpers","objet="+obj.getInt("nbAste"));
                Log.d("testpers","nbAstero="+Constante.NB_ASTEROIDE);
                Constante.VITESSE_MIN_ASTEROIDE=obj.getInt("vMIn");         // /-\
                Constante.VITESSE_MAX_ASTEROIDE=obj.getInt("vMax");         //  |---set les contante,
                Constante.TAILLE_MIN_ASTEROIDE=obj.getInt("tMin");          //  |   après on y touche plus
                Constante.TAILLE_MAX_ASTEROIDE=obj.getInt("tMax");          // \_/

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
}
