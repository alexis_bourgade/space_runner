package com.example.space_runner.View.Activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import androidx.annotation.Nullable;
import com.example.space_runner.Gestionnaire.AdaptateurSkin;
import com.example.space_runner.Gestionnaire.GestionnaireSauvegarde;
import com.example.space_runner.R;
import com.example.space_runner.Model.Vaisseau;

import java.util.ArrayList;


public class Skin extends Activity {
    private GestionnaireSauvegarde gestionnaireSauvegarde;

    private ArrayList<Vaisseau> listSkin;
    private ListView listVaisseau;
    /**
     * Constructeur
     * @param savedinstanceState est le Bundle à charger
     */
    @Override
    protected void onCreate(@Nullable Bundle savedinstanceState){
        super.onCreate(savedinstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.skin);
        gestionnaireSauvegarde = new GestionnaireSauvegarde(this,"gameSetting", Context.MODE_PRIVATE);

        listSkin = new ArrayList<>();
        listSkin.add(new Vaisseau("Petit Bleu",R.drawable.vaisseau1));
        listSkin.add(new Vaisseau("Moyen Bleu",R.drawable.vaisseau4));
        listSkin.add(new Vaisseau("Viking",R.drawable.vaisseau2));
        listSkin.add(new Vaisseau("Petit Rouge",R.drawable.vaisseau3));
        listSkin.add(new Vaisseau("Moyen Rouge",R.drawable.vaisseau6));
        listSkin.add(new Vaisseau("Petit Vert",R.drawable.vaisseau5));
        listSkin.add(new Vaisseau("Moyen Vert",R.drawable.vaisseau7));
        listSkin.add(new Vaisseau("Gros Vert",R.drawable.vaisseau8));

        AdaptateurSkin adapter = new AdaptateurSkin(this, R.layout.fragment_skin_vaisseau, listSkin);
        listVaisseau = findViewById(R.id.list_skin);
        listVaisseau.setAdapter(adapter);
        listVaisseau.setOnItemClickListener(listview_listerner);
    }

    /**
     * Permet d'interagir avec la liste de Vaisseau
     */
    AdapterView.OnItemClickListener listview_listerner = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
            gestionnaireSauvegarde.editorPutInt("skinSelected",listSkin.get(position).getImage());
        }
    };

    /**
     * Ferme l'activité
     * @param view est la vue courante
     */
    public void clicMenu(View view) { finish(); }

}
