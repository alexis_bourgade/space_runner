package com.example.space_runner.View;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import androidx.annotation.Nullable;
import com.example.space_runner.Constante;
import com.example.space_runner.R;
import com.example.space_runner.Model.Asteroide;
import com.example.space_runner.Model.BossMuta;
import com.example.space_runner.Gestionnaire.GestionnaireSauvegarde;
import com.example.space_runner.Model.JetDeBile;
import com.example.space_runner.Model.Missile;
import com.example.space_runner.Model.Vaisseau;
import com.example.space_runner.View.Activity.Play;

import java.util.ArrayList;


public class GameMaster extends View {

    //gestion des elements
    private Vaisseau vaisseau;
    private BossMuta muta;
    private Boolean isAttaqueActive=false;
    private ArrayList<Asteroide> arrAsteroide;
    private ArrayList<Missile> arrMissileJoueur;
    private ArrayList<JetDeBile> arrJetDeBile;

    //gestion du delay d'actualisation
    private final Handler handler;
    private final Runnable runnable;

    //gestion des textes
    private int distanceParcouru, bestScore=0, distanceDernierBoss=0;
    private Play play;

    //gestion du jeux
    private boolean run;
    private GestionnaireSauvegarde gs;

    /**
     * Constructeur
     * @param context est le contexte de l'application
     * @param attrs est une collection d'attributs
     */
    public GameMaster(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        run=false;
        gs = new GestionnaireSauvegarde(context, "gameSetting",Context.MODE_PRIVATE);
        if(gs.getSharedPreferences()!=null){
            bestScore = gs.getValueIntSharedPreference("bestScoreSave",0);
            Log.d("bestScoreSave", String.valueOf(bestScore));
        }
        initVaisseau();
        initAsteroide();
        initBossMuta();

        handler = new Handler();
        //pour update la methode "draw"
        runnable = this::invalidate;
    }

    /**
     * Initialise le vaisseau pour la partie
     */
    public void initVaisseau(){
        vaisseau = new Vaisseau();
        vaisseau.setHeight(Constante.HEIGHT_SCREEN/10);
        vaisseau.setWidth(Constante.WIDTH_SCREEN/10);
        vaisseau.setX((float)Constante.WIDTH_SCREEN/10);
        vaisseau.setY(((float)Constante.HEIGHT_SCREEN/2) - (float)(vaisseau.getHeight()/2));
        ArrayList<Bitmap>  listAnimation = new ArrayList<>();
        int skinSelected = gs.getValueIntSharedPreference("skinSelected",3);
        if (skinSelected==3){
            listAnimation.add(BitmapFactory.decodeResource(this.getResources(),R.drawable.vaisseau1));
        }
        else {
            listAnimation.add(BitmapFactory.decodeResource(this.getResources(), skinSelected));
        }
        vaisseau.setListAnnimation(listAnimation);
    }


    /**
     * Initiaiise le boss Mutalisk pour la partie
     */
    public void initBossMuta(){
        Log.d("testBoss","init");
        muta = new BossMuta();
        muta.setHeight(Constante.HEIGHT_SCREEN/3);
        muta.setWidth(Constante.WIDTH_SCREEN/3);
        muta.setX(Constante.WIDTH_SCREEN);
        muta.setY((Constante.HEIGHT_SCREEN >> 1) - (muta.getHeight() >> 1));
        ArrayList<Bitmap>  listAnimation = new ArrayList<>();
        listAnimation.add(BitmapFactory.decodeResource(this.getResources(), R.drawable.muta1));
        listAnimation.add(BitmapFactory.decodeResource(this.getResources(), R.drawable.muta2));
        listAnimation.add(BitmapFactory.decodeResource(this.getResources(), R.drawable.muta3));
        listAnimation.add(BitmapFactory.decodeResource(this.getResources(), R.drawable.muta4));
        listAnimation.add(BitmapFactory.decodeResource(this.getResources(), R.drawable.muta5));
        listAnimation.add(BitmapFactory.decodeResource(this.getResources(), R.drawable.muta4));
        listAnimation.add(BitmapFactory.decodeResource(this.getResources(), R.drawable.muta3));
        listAnimation.add(BitmapFactory.decodeResource(this.getResources(), R.drawable.muta2));
        muta.setListAnnimation(listAnimation);
    }

    /**
     * Initialise les asteroïde pour la partie
     */
    public void initAsteroide() {
        arrAsteroide = new ArrayList<>();
        for (int i=0; i < Constante.NB_ASTEROIDE; i++ ){
            arrAsteroide.add(new Asteroide());
            arrAsteroide.get(arrAsteroide.size()-1).setBm(BitmapFactory.decodeResource(this.getResources(),R.drawable.asteroide1));
        }
    }

    /**
     * Initialise la liste d'attaque du joueur et du boss pour la partie
     */
    public void initAttaque(){
        arrMissileJoueur = new ArrayList<>();

        arrMissileJoueur.add(new Missile(vaisseau.getX(),vaisseau.getY()));
        arrMissileJoueur.get(arrMissileJoueur.size()-1).setBm(BitmapFactory.decodeResource(this.getResources(),R.drawable.missile));

        arrJetDeBile = new ArrayList<>();
        arrJetDeBile.add(new JetDeBile(muta.getX(),vaisseau.getY(),muta));
        arrJetDeBile.get(arrJetDeBile.size()-1).setBmMatrixed(BitmapFactory.decodeResource(this.getResources(),R.drawable.attack_muta));
    }

    /**
     * Dessine la partie
     */
    @Override
    public void draw(Canvas canvas){
        super.draw(canvas);
        if(run){
            vaisseau.draw(canvas);

            if (distanceParcouru-distanceDernierBoss>10000){
                boss(canvas);
            }
            if(!muta.isBossAcitve()){
                addDistanceParcouru(Constante.DISTANCE);
                play.setDistanceParcouru(distanceParcouru+"m");
            }
            colisionAsteroide(canvas);
        }
        else{
            vaisseau.drawInit(canvas);
        }
        handler.postDelayed(runnable,10);
    }

    /**
     * Déclanche le boss
     */
    private void boss(Canvas canvas){
        if (muta.getPv()>5) declancherDebutBossMuta(canvas);
        else{
            declancherFinBossMuta(canvas);
        }
    }


    private void declancherDebutBossMuta(Canvas canvas) {
        if (muta.getX()>Constante.WIDTH_SCREEN-600){
            muta.setArrive(true);
            muta.setBossAcitve(true);
        }
        else{
            muta.setArrive(false);
            if (!isAttaqueActive){
                isAttaqueActive=true;
                play.affichageBoss(View.VISIBLE);
                initAttaque();
            }
            drawAttaqueVaisseau(canvas);
            if (muta.getNiveau()>1){
                drawAttaqueBoss(canvas);
            }
        }
        muta.draw(canvas);
    }

    private void declancherFinBossMuta(Canvas canvas) {
        if (muta.getX()<Constante.WIDTH_SCREEN+(float)muta.getWidth()/2){ muta.setPart(true); }
        else{
            Log.d("testBoss3","fin");
            muta.setPart(false);
            muta.setBossAcitve(false);
            muta.lvlUp();
            muta.resetBoss();
            play.affichageBoss(View.INVISIBLE);
            play.actualiserBarrePv();
            distanceDernierBoss=distanceParcouru;
        }
        isAttaqueActive=false;

        muta.draw(canvas);

    }

    public void drawAttaqueVaisseau(Canvas canvas){
        Missile att;
        for(int i = 0; i< arrMissileJoueur.size(); i++){
            att= arrMissileJoueur.get(i);
            att.draw(canvas);

            if (muta.getRect().intersect(att.getRect())){
                Log.d("itbox","touché"+muta.getNiveau());
                muta.prendDegat();
                play.actualiserBarrePv();
            }
            if (att.getX() > Constante.WIDTH_SCREEN+att.getWidth() || att.getRect().intersect(muta.getRect())){
                arrMissileJoueur.remove(att);
            }
            if ((float)Constante.WIDTH_SCREEN/2<=att.getX() && att.getX()<((float) Constante.WIDTH_SCREEN/2)+30){
                arrMissileJoueur.add(new Missile(vaisseau.getX(),vaisseau.getY()));
                arrMissileJoueur.get(arrMissileJoueur.size()-1).setBm(BitmapFactory.decodeResource(this.getResources(),R.drawable.missile));
            }
        }
    }

    public void drawAttaqueBoss(Canvas canvas){
        JetDeBile jet;
        for (int i=0; i< arrJetDeBile.size() ; i++){
            jet = arrJetDeBile.get(i);
            jet.draw(canvas);
            if (vaisseau.getRect().intersect(jet.getRect())){
                collision();
            }
            if (jet.getX() <= 0){
                Log.d("testAttaqueBoss","dois creer un nouveau jet de bile"+muta.getX());
                arrJetDeBile.add(new JetDeBile(muta.getX(),vaisseau.getY(),muta));
                arrJetDeBile.get(arrJetDeBile.size()-1).setBmMatrixed(BitmapFactory.decodeResource(this.getResources(),R.drawable.attack_muta));

                arrJetDeBile.remove(jet);
            }
        }
    }

    public void addDistanceParcouru(int distance){
        distanceParcouru+=distance;
    }

    public  void colisionAsteroide(Canvas canvas){
        Asteroide a;
        for(int i=0; i<Constante.NB_ASTEROIDE;i++){
            if (vaisseau.getRect().intersect(arrAsteroide.get(i).getRect())){
                collision();
            }
            a=arrAsteroide.get(i);
            if (a.getX() < -a.getWidth()){
                a.setX(Constante.WIDTH_SCREEN);
                a.randomYDepart();
                a.setRandomVitesseDep();
                a.setHeight();
            }
            a.draw(canvas);
        }
    }

    public void collision(){
        run=false;
        if (distanceParcouru>bestScore){
            bestScore=distanceParcouru;
            gs.editorPutInt("bestScoreSave",bestScore);
        }

        play.affichageBoss(View.INVISIBLE);
        play.afficherGameOver(View.VISIBLE);
    }

    public Vaisseau getVaisseau() { return vaisseau; }

    public void setRun(boolean start) { this.run = start;
    }

    public boolean isRun() { return !run; }

    public int getDistanceParcouru() { return distanceParcouru; }

    public void setDistanceParcouru(int distanceParcouru) {
        this.distanceParcouru = distanceParcouru;
        this.distanceDernierBoss = distanceParcouru;
    }

    public int getBestScore() { return bestScore; }

    public void setPlay(Play play) { this.play=play; }

    public float getPvBoss(){ return muta.getPv(); }

    public void setAttaqueActive(Boolean attaqueActive) { isAttaqueActive = attaqueActive; }
}
