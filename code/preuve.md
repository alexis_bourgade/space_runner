# Preuves

#### Je sais utiliser les Intent pour faire communiquer deux activités.

Pour lancer une partie, on doit recuperer le niveau selectionné par le joueur.

par exemple si on selectionne le niveau facile, la communication s'effectue à la ligne 78 du fichier Menu.java 
puis est recupéré à la ligne 51 du fichier PLay.java, à la creation du GestionnaireNiveau

Menu.java :
```java
public void facile(View view){
        Intent playActivity = new Intent(this, Play.class);
        playActivity.putExtra("niveau",0);
        startActivity(playActivity);
    }
```
Play.java :
```java
new GestionnaireNiveau(this, this.getIntent().getIntExtra("niveau",1));
```
#### Je sais développer en utilisant le SDK le plus bas possible.
Nous avons réussi à descendre à la SDK 16 qui couvre 99.8% des appareils   


#### Je sais distinguer mes ressources en utilisant les qualifier 
Les images sont dans le dossiers drawable, les fichiers « raw », c’est à dire les fichiers musiques et text sont dans le dossier raw, les layout dans les layout etc.

#### Je sais modifier le manifeste de l'application en fonction de mes besoins 
Nous avons du modifier le fichier manifest pour ajouter des vues et modifier l'orientation de l'écran.

```java
<activity android:name=".view.Menu">
        <intent-filter>
             <action android:name="android.intent.action.MAIN" />
             <category android:name="android.intent.category.LAUNCHER" />
	</intent-filter>
</activity>

<activity android:name=".view.Play"
        android:screenOrientation="landscape">
 </activity>

<activity android:name=".view.Option"/>
<activity android:name=".view.Skin"/>
```

Mais aussi pour changer l'icon de l'application et son nom

```java
android:icon="@mipmap/ic_launcher"
android:label="@string/app_name"
android:roundIcon="@mipmap/ic_launcher_round"
```

#### Je sais faire des vues xml en utilisant layouts et composants adéquats 
Nous le constraintlayout qui permet de mettre nos canvas dans activity_play par exemple.
Pour les fragments, nous utilisons des RelativeLayouts afin de s'adapter au mieu au different écran

#### Je sais coder proprement mes activités, en m'assurant qu'elles ne font que relayer les évènements 
Par exemple dans GameActivity, les evenements "onTouch" sont envoyer au GameMaster

```java
upButton.setOnTouchListener((View v, MotionEvent event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
               gameMaster.getVaisseau().setBoolUp(true);
        }
	else if (event.getAction() == MotionEvent.ACTION_UP) {
               gameMaster.getVaisseau().setBoolUp(false);
        }
        return true;
});

downButton.setOnTouchListener((View v, MotionEvent event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                gameMaster.getVaisseau().setBoolDown(true);

        }
	else if (event.getAction() == MotionEvent.ACTION_UP) {
                gameMaster.getVaisseau().setBoolDown(false);
        }
        return true;
});
```

#### Je sais coder une application en ayant un véritable métier 
Notre métier est GameMaster, aucune vue n'est présente à l'interieur

#### Je sais parfaitement séparer vue et modèle 
Les vues sont dans le package view et les modeles dans le package model. Aucune vue n'est présente à l'intérieur du modèle et l'inverse est vrai aussi. La vue envois les événements au modéle lui correspondant et le modèle traite ces données là.

#### Je maîtrise le cycle de vie de mon application 
Dans le fichier Play, il y a "onResume" qui permet de relancer la musique après une pause de l'application. On lance aussi le "registerListener" pour commencer à capturer les valeurs de l'acceleromètre, on recalcule par ailleur sa position de base.
Dans "onPause", on arrète la musique et la prise de position de l'acceleromètre.  
```java
 @Override
    protected void onResume() {
        super.onResume();
        if (isAcceleromètreActive) {
            acceleroManager.registerListener(gestioAccelero.getAcceleroEventListener(), gestioAccelero.getAccelero(), SensorManager.SENSOR_DELAY_FASTEST);
        }
        if (isMusicActive) mediaPlayer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isAcceleromètreActive){
            acceleroManager.unregisterListener(gestioAccelero.getAcceleroEventListener());
        }
        if (isMusicActive) mediaPlayer.pause();
    }
```

#### Je sais utiliser le findViewById à bon escient 
Nous utilisons les findViewById notemment dans Play.java pour mettre à jour les differente TextView, Progressbar tels que distance parcourue et la barre de vie du boss et aussi pour lier nos bouton

```java
gameMaster = findViewById(R.id.gameView1);
fragAffichageBoss = findViewById(R.id.fragmentPvBar);
fragAffichageBoss.setVisibility(View.INVISIBLE);
gameOver = findViewById(R.id.fragmentGameOver);

.....

score = gameOver.findViewById(R.id.score);
txt_gameOver = gameOver.findViewById(R.id.txt_gameOver);
retourMenu = gameOver.findViewById(R.id.buttonMenu);
reJouer = gameOver.findViewById(R.id.buttonReJouer);
barPv = fragAffichageBoss.findViewById(R.id.pvBar);
```

#### Je sais gérer les permissions dynamiques de mon application 

Notre application est un jeu qui ne demande en rien d'acceder à des fonctions spécifiques du téléphone, nous n'en avons donc pas l'utilité.
Néamoins, pour en ajouter il suffit de mettre  au début du fichier manifest:

```
< uses-permission android:name="android.permission.NOMPERMISSION"/>

```

Pour la camera, ce serait:

```
< uses-permission android:name="android.permission.CAMERA"/>

```

On peut ensuite vérifier que l'application a bien le droit d'acceder à la camera
```java
if (ContextCompat.checkSelfPermission(thisActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
	// l application n'a pas accés à internet
}

```

#### Je sais gérer la persistance légère de mon application 
Nous avons une classe GestionnaireSauvegarde qui utilise un SharedPreferences afin de garder en mémoire si la musique et l'aceleromètre sont activés, ainsi que le skin choisi par le joueur. 

private final SharedPreferences sp;

public GestionnaireSauvegarde(Context context, String nom, int mode){
    sp = context.getSharedPreferences(nom, mode);
}

Pour stocker un int dans le SharedPreferences : 
```
public void editorPutInt(String key, int value){
    SharedPreferences.Editor editor = sp.edit();
    editor.putInt(key,value);
    editor.apply();
}
```

Pour récupérer la valeur stockée : 
```
public int getValueIntSharedPreference(String key, int defautValue){ return sp.getInt(key,defautValue); }
```


#### Je sais gérer la persistance profonde de mon application 


Le paramètrage des niveaux se trouve dans dans assets/niveau.jSon .
Il est chargé grace au Gestionnaireniveau qui se trouve dans Gestionnaire.

#### Je sais afficher une collection de données
Nous affichons une collection de données dans le menu "Skin", nous affichon un liste avec notre adaptateur et nos fragments.
On interagie avec celui-ci car il nous permet de selectionner un skin.

```java
OnItemClickListener listview_listerner = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
            gestionnaireSauvegarde.editorPutInt("skinSelected",listSkin.get(position).getImage());
        }
```


#### Je sais coder mon propre adaptateur 

Nous avons codé notre propre adapteur pour afficher les skin, pour cela , notre model est le suivant :

```java
class Vaisseau {
	private String nom;
    	private int image;


	public Vaisseau(String nom, int image){
        this.nom=nom;
        this.image=image;
    }
}
```

Nous utilisons un fragment pour afficher chaque vaisseau:
```
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:orientation="vertical">
    <ImageView
        android:id="@+id/imageVaisseau"
        android:minWidth="100dp"
        android:layout_width="wrap_content"
        android:layout_height="60dp"
        android:layout_margin="6dp">
    </ImageView>
    <TextView
        android:id="@+id/nomVaisseau"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginVertical="20dp"
        android:layout_marginLeft="10dp"
        android:layout_marginStart="10dp"
        android:textSize="26sp">
    </TextView>
</LinearLayout>
```

Et une classe "AdaptateurSkin" qui herite de ArrayAdapter

```java
public class AdaptateurSkin extends ArrayAdapter<Vaisseau> {


    private final Context _context;
    private ArrayList<Vaisseau> vaisseau;

    public AdaptateurSkin(Context context, int resource, ArrayList<Vaisseau> vaisseau) {
        super(context, resource, vaisseau);
        _context = context;
        this.vaisseau = vaisseau;
    }

    @Override
    public View getView(int position, View vaisseauView,  ViewGroup parent) {
        if(vaisseauView == null) {
            LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vaisseauView = inflater.inflate(R.layout.fragment_skin_vaisseau, parent, false);
        }
        ImageView imageView = vaisseauView.findViewById(R.id.imageVaisseau);
        imageView.setBackgroundResource(vaisseau.get(position).getImage());

        TextView viewTitle = vaisseauView.findViewById(R.id.nomVaisseau);
        Log.d("testAdapteteur", vaisseau.get(position).getNom());
        viewTitle.setText(vaisseau.get(position).getNom());
        viewTitle.setTag(vaisseau.get(position).getNom());
        return vaisseauView;
    }
}
```
Nous créons ensuite une list de vaisseau dans l'activité que l'on lie à l'adaptateur

```java
	ArrayList<Vaisseau> listSkin = new ArrayList<>();
 	listSkin.add(new Vaisseau("Petit Bleu",R.drawable.vaisseau1));
        listSkin.add(new Vaisseau("Moyen Bleu",R.drawable.vaisseau4));
	.....
        listSkin.add(new Vaisseau("Viking",R.drawable.vaisseau2));
        listSkin.add(new Vaisseau("Petit Rouge",R.drawable.vaisseau3));

	AdaptateurSkin adapter = new AdaptateurSkin(this, R.layout.fragment_skin_vaisseau, listSkin);
        listVaisseau = findViewById(R.id.list_skin);
        listVaisseau.setAdapter(adapter);

        listVaisseau.setOnItemClickListener(listview_listerner);

```

Et pour finir on crée un méthode OnItemClickListener pour pouvoir interagir avec nos vaisseaux

```java
OnItemClickListener listview_listerner = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
            gestionnaireSauvegarde.editorPutInt("skinSelected",listSkin.get(position).getImage());
        }
    };
```

#### Je maîtrise l'usage des fragments 
Nous avons utilisé des fragments afin d'afficher divers élément de nos vue, tels que le game over ou la bar de vie du boss.
Nous les utilisons aussi pour la afficher les skin de la liste de skin.
les fragment se trouve dans View/Fragment

#### Je maîtrise l'utilisation de GIT
Nous avons essayés de faire le plus de brache possible quand cela nous semblait nécessaire, nous feusions des commit regulier dans qu'un fonctionnalité était au point, sans forcement push.
Nous avons aussi essayer de travailler le moins possible sur les mêmes fichiers afin de faciliter les merges

### Application

#### Je sais utiliser l'accéléromètre

Notre fichier gérant l'acceleromètre se trouve dans Gestionnaire/GestionnaireAccelero
